package state;

import java.util.Arrays;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * 
 * A class to handle the saving of puzzle state and validating the load state.
 * 
 * @author Rick
 *
 */
public class SaveStateDb extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "state.db";

	// Contacts table name
	private static final String STATE_TABLE = "state3";

	// Contacts Table Columns names
	private static final String COLUMN_ID = "ID";
	private static final String COLUMN_DIFFICULTY = "DIFFICULTY";
	private static final String COLUMN_IMAGENUMBER = "IMAGENUMBER";
	private static final String COLUMN_SLOTS = "SLOTS";
	private static final String COLUMN_SOUND = "SOUND";
	private static final String COLUMN_CHIME = "CHIME";
	private static final String COLUMN_MUSIC = "MUSIC";
	private static final String COLUMN_BORDER = "BORDER";

	private static final String DB_CREATE = "CREATE TABLE IF NOT EXISTS "
			+ STATE_TABLE + " (" + COLUMN_ID + " INTEGER PRIMARY KEY,"
			+ COLUMN_DIFFICULTY + " INTEGER," + COLUMN_IMAGENUMBER
			+ " INTEGER," + COLUMN_SLOTS + " VARCHAR," + COLUMN_SOUND
			+ " INTEGER," + COLUMN_CHIME + " INTEGER," + COLUMN_MUSIC
			+ " INTEGER," + COLUMN_BORDER + " INTEGER" + ")";

	public SaveStateDb(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DB_CREATE);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + STATE_TABLE);

		// Create tables again
		onCreate(db);
	}

	/**
	 * All CRUD(Create, Read, Update, Delete) Operations
	 */

	// Adding new contact
	public void saveState(int difficulty, int imageNumber, String slotString,
			boolean playSetSound, boolean playChimeSound, boolean playMusic,
			boolean drawBorders) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();

		boolean saveOk = true;
		// check for slots to be set
		String[] slotArr = slotString.split(",");
		int[] intSlotArr = new int[slotArr.length];
		for (int i = 0; i < intSlotArr.length; i++) {
			intSlotArr[i] = Integer.parseInt(slotArr[i]);
		}

		// sort array and check for each number in it
		Arrays.sort(intSlotArr);
		for (int i = 0; i < intSlotArr.length; i++) {
			if (intSlotArr[i] != i)
				saveOk = false;
		}

		if (saveOk) {
			values.put(COLUMN_DIFFICULTY, difficulty);
			values.put(COLUMN_IMAGENUMBER, imageNumber);
			values.put(COLUMN_SLOTS, slotString);
			values.put(COLUMN_SOUND, (playSetSound) ? 1 : 0);
			values.put(COLUMN_CHIME, (playChimeSound) ? 1 : 0);
			values.put(COLUMN_MUSIC, (playMusic) ? 1 : 0);
			values.put(COLUMN_BORDER, (drawBorders) ? 1 : 0);
		}

		db.insert(STATE_TABLE, null, values);
		db.close(); // Closing database connection
	}

	// Getting contacts Count
	public void loadState() {
		boolean resumePreviousPuzzle = false;

		SQLiteDatabase stateDB = this.getReadableDatabase();

		stateDB.execSQL(DB_CREATE);

		Cursor c = stateDB.query(STATE_TABLE, null, null, null, null, null,
				null);

		if (c.moveToLast()) {
			// check for valid slots to be set
			String[] slotArr = c.getString(3).split(",");
			int[] intSlotArr = new int[slotArr.length];
			for (int i = 0; i < intSlotArr.length; i++) {
				intSlotArr[i] = Integer.parseInt(slotArr[i]);
			}

			// sort array and check for each number in it
			Arrays.sort(intSlotArr);
			resumePreviousPuzzle = true;
			for (int i = 0; i < intSlotArr.length; i++) {
				if (intSlotArr[i] != i)
					resumePreviousPuzzle = false;
			}

			if (resumePreviousPuzzle) {
				// game will restart with old objects
				CommonVariables cv = CommonVariables.getInstance();
				cv.difficulty = c.getInt(1);
				cv.currentPuzzleImagePosition = c.getInt(2);
				cv.setSlots(c.getString(3));
				cv.playTapSound = c.getInt(4) > 0;
				cv.playChimeSound = c.getInt(5) > 0;
				cv.playMusic = c.getInt(6) > 0;
				cv.drawBorders = c.getInt(7) > 0;
				cv.resumePreviousPuzzle = true;
			}

			c.close();

		}
	}

	public void delete() {
		// after attempt to load delete the record
		SQLiteDatabase stateDB = this.getReadableDatabase();
		stateDB.execSQL("DROP TABLE IF EXISTS " + STATE_TABLE);
		stateDB.close();
	}
}
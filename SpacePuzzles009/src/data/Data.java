package data;

import nasa.puzzles.R;

/**
 * A class to hold the static data used across the application.
 * 
 * @author Rick
 * 
 */
public class Data {

	public final static String[] PIC_NAMES = { "Blue Marble", "Ozone Hole",
			"Satelite Climate", "Earth Moon", "Atlantis", "Mars", "Hubble 01",
			"Apllo 16", "Lagoon Nebula", "Dusty Spiral", "Tarantula Nebula",
			"Steller", "Cat's Eye Nebula", "Glowing Eye", "Distant Spiral",
			"X-class Flare", "Neptune and Triton", "Sun", "Supermoon",
			"Supernova", "Red Amazon", "Blue Sun", "Gold Sun", "Hubble 02",
			"MMU", "Stephens Quintet", "Neptune" };

	public final static String[] PIC_URLS = {
			"https://www.flickr.com/photos/nasacommons/9467448026/in/set-72157634981806429/",
			"https://www.flickr.com/photos/nasacommons/9467446738/in/set-72157634981806429/",
			"https://www.flickr.com/photos/nasacommons/9464665031/in/set-72157634981806429/",
			"https://www.flickr.com/photos/nasacommons/9464664545/in/set-72157634981806429/",
			"https://www.flickr.com/photos/nasacommons/9459340606/in/set-72157634975731504",
			"https://www.flickr.com/photos/nasacommons/7651156426/in/set-72157630766688018",
			"https://www.flickr.com/photos/nasacommons/9461079254/in/set-72157634975233898",
			"https://www.flickr.com/photos/nasacommons/9457456433/in/set-72157634974031758",
			"https://www.flickr.com/photos/nasacommons/9458008327/in/set-72157634975233898",
			"https://www.flickr.com/photos/nasacommons/9460799092/in/set-72157634975233898/",
			"https://www.flickr.com/photos/nasacommons/9458014663/in/set-72157634975233898/",
			"https://www.flickr.com/photos/nasacommons/9460796504/in/set-72157634975233898/",
			"https://www.flickr.com/photos/nasacommons/9464528703/in/set-72157634975233898/",
			"https://www.flickr.com/photos/nasacommons/9464532639/in/set-72157634975233898/",
			"https://www.flickr.com/photos/nasacommons/9464531387/in/set-72157634975233898/",
			"https://www.flickr.com/photos/gsfc/13944756840/in/set-72157644150414868",
			"https://www.flickr.com/photos/nasacommons/9464660003/in/set-72157634981806429",
			"https://www.flickr.com/photos/gsfc/7931832934/in/set-72157642013369213/",
			"https://www.flickr.com/photos/gsfc/9101429325/in/set-72157642013369213/",
			"https://www.flickr.com/photos/gsfc/4420216245/in/set-72157642013369213/",
			"https://www.flickr.com/photos/gsfc/13105448304/in/set-72157623639063326",
			"https://www.flickr.com/photos/gsfc/4721839258/in/set-72157624201177587",
			"https://www.flickr.com/photos/gsfc/4721839490/in/set-72157624201177587/",
			"https://www.flickr.com/photos/gsfc/4545825554/in/set-72157623915634530",
			"https://www.flickr.com/photos/nasacommons/9449352571/in/set-72157634951821461",
			"https://www.flickr.com/photos/gsfc/3904168904/in/set-72157642013369213",
			"https://www.flickr.com/photos/nasacommons/9467450366/in/set-72157634981806429" };

	public final static int[] PICS = { R.drawable.image0, R.drawable.image1,
			R.drawable.image2, R.drawable.image3, R.drawable.image4,
			R.drawable.image5, R.drawable.image6, R.drawable.image7,
			R.drawable.image8, R.drawable.image9, R.drawable.image10,
			R.drawable.image11, R.drawable.image12, R.drawable.image13,
			R.drawable.image14, R.drawable.image15, R.drawable.image16,
			R.drawable.image17, R.drawable.image18, R.drawable.image19,
			R.drawable.image20, R.drawable.image21, R.drawable.image22,
			R.drawable.image23, R.drawable.image24, R.drawable.image25,
			R.drawable.image26 };

	public final static String DEVART_LINK = "http://freemusicarchive.org/music/Kevin_MacLeod/";
	public final static String ARTIST = "NASA";
	public static String PATH = "android.resource://nasa.puzzles/";
	public static int TRACK_01 = R.raw.track01;

}
